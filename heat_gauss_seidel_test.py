import numpy as np

# create domain
nrow = 101
ncol = 101
cellsize = 1
k = 2


k = np.full((ncol, nrow), 1e-3)
k_alt = np.full((101, 51), 5e-3)
k[0:101, 50:101] = k_alt


# initial and boundary conditions
top = 25.
bottom = 20.
initial = 22.

temperature = np.tile(initial, (ncol,nrow))
for i in range(ncol):        
	temperature[0] = top
	temperature[(nrow-1)] = bottom
print(temperature)

# create wells matrix
wells = np.zeros((ncol, nrow))
wells[50, 40] = 0.02
wells[50, 45] = 0.02
wells[50, 50] = 0.02
wells[50, 55] = 0.02
wells[50, 60] = 0.02
wells[40, 50] = 0.02

print(wells)


print("Temperature matrix is a ", nrow, "by ", ncol, " matrix.")
iterations = 1
conv_crit = 1e-5
converged = False
while (not converged):

	for r in range(1, nrow - 1):
		for c in range(1, ncol - 1):
			temperature_old = temperature[r, c]
			temperature[r, c] = (k[r, c] + ((temperature[r - 1, c] + temperature[r + 1, c] + temperature[r, c - 1] + temperature[r, c + 1]) + (2*wells[r, c]*cellsize/k[r, c]))) / 4.   
	
	#left boundary		
	for r in range(1, nrow - 1):
		c = 0
		temperature_old = temperature[r, c]
		temperature[r, c] = (temperature[r - 1, c] + temperature[r + 1, c] + 2*temperature[r, c + 1]) / 4.
		
	#right boundary
	for r in range(1, nrow -1):
		c = ncol - 1
		temperature_old = temperature[r, c]
		temperature[r, c] = (temperature[r - 1, c] + temperature[r + 1, c] + 2*temperature[r, c - 1]) / 4.
		
	diff = temperature[r, c] - temperature_old
	diff2 = temperature_old - temperature[r, c]
	if (diff < conv_crit) and (diff2 < conv_crit):
		converged = True
	iterations += iterations 
print("Number of iterations = ", iterations - 1)
print(temperature)

#plot results
import matplotlib.pyplot as plt
X = np.linspace(0, nrow*cellsize, nrow)
Y = np.linspace(nrow*cellsize, 0, nrow)
levels = 15
fig, ax = plt.subplots(1, 1)
cp = ax.contour(X, Y, temperature, levels)
fig.colorbar(cp)
ax.set_title('Calculated temperature')
ax.set_xlabel('X (m)')
ax.set_ylabel('Y (m)')
plt.show()


fig, ax = plt.subplots(figsize=(10,10))
im = ax.imshow(temperature)





