import os
import numpy as np
import matplotlib.pyplot as plt
import flopy

name = "unconfined_mf6"
initial = 7.
h = 7.
nlay = 1
nrow = 11
ncol = 11
delrow = delcol = 100
#H = 100
k = 2.0
top = 9
bottom = 5


sim = flopy.mf6.MFSimulation(sim_name=name, exe_name='/opt/modflow/mf6', version="mf6", sim_ws=".")
tdis = flopy.mf6.ModflowTdis(sim, pname="tdis", time_units="DAYS", nper=1, perioddata=[(1.0, 1, 1.0)])
ims = flopy.mf6.ModflowIms(sim, pname="ims", complexity="SIMPLE")

gwf = flopy.mf6.ModflowGwf(sim, modelname=name)
dis = flopy.mf6.ModflowGwfdis(gwf, nlay=nlay, nrow=nrow, ncol=ncol, delr=delrow, delc=delcol)

# initial conditions
start = h * np.ones((nlay, nrow, ncol))
ic = flopy.mf6.ModflowGwfic(gwf, pname="ic", strt=start)

npf = flopy.mf6.ModflowGwfnpf(gwf, icelltype=1, k=k, save_flows=True)

chd_rec = []
for layer in range(0, nlay):
    for i in range(0, ncol):
        chd_rec.append(((layer, 0, i), top))
        chd_rec.append(((layer, nrow-1, i), bottom))
print(chd_rec)
        
lrcQ = {0: [[0, 5, 5, 0]]} # well data


chd = flopy.mf6.ModflowGwfchd(gwf, stress_period_data=chd_rec, save_flows=True)   
well = flopy.mf6.ModflowGwfwel(gwf, stress_period_data=lrcQ)
rch = flopy.mf6.ModflowGwfrch(gwf, stress_period_data=[((0, 5, 5), -0.00108)])



headfile = "{}.hds".format(name)
head_filerecord = [headfile]
budgetfile = "{}.cbb".format(name)
budget_filerecord = [budgetfile]
saverecord = [("HEAD", "ALL"), ("BUDGET", "ALL")]
printrecord = [("HEAD", "LAST")]

oc = flopy.mf6.ModflowGwfoc(gwf, saverecord=saverecord, head_filerecord=head_filerecord, budget_filerecord=budget_filerecord, printrecord=printrecord)     

sim.write_simulation()
sim.run_simulation()

hds = flopy.utils.binaryfile.HeadFile(headfile)
h = hds.get_data(kstpkper=(0, 0))
x = y = np.linspace(0, ncol*delcol, ncol)
y = y[::-1]
fig = plt.figure(figsize=(6, 6))
ax = fig.add_subplot(1, 1, 1, aspect="equal")
c = ax.contour(x, y, h[0], np.arange(3.91, 9, 0.5), colors="black")
plt.clabel(c, fmt="%2.1f")

