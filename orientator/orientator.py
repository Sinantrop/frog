"""
this script is based on the drillcore_tramsformation module, created by Nicolas Ovaskainen. 
For more information about the drillcore_transformation module please visit: https://github.com/nialov/drillcore-transformations and https://drillcore-transformations.readthedocs.io/en/latest/index.html

"""
from drillcore_transformations.transformations import *
#from drillcore_transformations.visualizations import *

import tkinter as tk
from tkinter import filedialog
import tkinter.messagebox
from pandas import read_excel
from pandas import DataFrame
from numpy import concatenate


root= tk.Tk()
 
canvas = tk.Canvas(root, width = 700, height = 550)
canvas.pack()
 
def calcExcel(excel_file, output_directory):
    df = read_excel(excel_file)
    df = df.reset_index()
    df_num = df.to_numpy() 
    if df_num.shape[1] != 6:
        message = "Error: excelový soubor musí mít formát: hloubka|alfa|beta|trend|plunge"
        tkinter.messagebox.showerror('vypocet se nezdaril', message)
    else:
        pass
    input_entry.delete(0, tk.END) 
    output_entry.delete(0, tk.END)
        
    def calculate(alpha, beta, trend, plunge):
        plane_normal = calc_global_normal_vector(alpha, beta, trend, plunge)
        plane_dir, plane_dip = calc_plane_dir_dip(plane_normal)
        
        def cor_plane_dir(plane_dir_orig):
            if plane_dir_orig < 180:
                plane_dir_cor = plane_dir_orig + 180
            else:
                plane_dir_cor = plane_dir_orig - 180
            return plane_dir_cor
        plane_dir = cor_plane_dir(plane_dir)
        return(plane_dip, plane_dir)    
    
    array = np.zeros([df_num.shape[0], 2]) 
    for index, row in enumerate(df_num):
        array[index] = calculate(row[2], row[3], row[4], row[5])
       
    print(df_num.shape[1])
    concat = concatenate((df_num, array), axis=1)
    my_array = concat
    df_2 = DataFrame(my_array, columns = ['index', 'hloubka', 'alfa', 'beta', 'trend', 'plunge', 'dip', 'dir'])
    df_2.to_excel(output_directory, index=False)
    message = ":-)"
    tkinter.messagebox.showinfo('vypocet se zdaril', message)
    
def vypocet():
    excel_file = input_entry.get()
    output_directory = output_entry.get()
    calcExcel(excel_file, output_directory)
    input_entry.delete(0, tk.END) 
    output_entry.delete(0, tk.END)

def input():
    input_path = tk.filedialog.askopenfilename(filetypes = (("xls files","*.xls *.xlsx"),("all files","*.*")))
    input_entry.delete(1, tk.END)  
    input_entry.insert(0, input_path)  

def output():
    output_path = tk.filedialog.asksaveasfilename(filetypes = (("xlsx file","*.xlsx"), ("all files","*.*")))
    output_entry.delete(1, tk.END)  
    output_entry.insert(0, output_path)  
 
def manual():           
    plane_normal = calc_global_normal_vector(float(alpha_m.get()), float(beta_m.get()), float(trend_m.get()), float(plunge_m.get()))
    plane_dir, plane_dip = calc_plane_dir_dip(plane_normal)
    
    def cor_plane_dir(plane_dir_orig):
        if plane_dir_orig < 180:
            plane_dir_cor = plane_dir_orig + 180
        else:
            plane_dir_cor = plane_dir_orig - 180
        return plane_dir_cor
    plane_dir = cor_plane_dir(plane_dir)
    
    sklon = plane_dip
    azimut = plane_dir
    
    blank_1.configure(state='normal')
    blank_2.configure(state='normal')
    blank_1.delete(0, tk.END)
    blank_2.delete(0, tk.END)
    blank_1.insert(0, sklon)
    blank_2.insert(0, azimut)
    blank_1.configure(state='readonly')
    blank_2.configure(state='readonly')
        
#======================================= rucne zadavanie =============================================    
alfaLabel = tk.Label(root, text="alfa")
canvas.create_window(50, 50, window = alfaLabel)   

betaLabel = tk.Label(root, text="beta")
canvas.create_window(50, 80, window = betaLabel)

trendLabel = tk.Label(root, text="azimut vrtu")
canvas.create_window(420, 50, window = trendLabel)

plungeLabel = tk.Label(root, text="sklon vrtu")
canvas.create_window(420, 80, window = plungeLabel)

azimutLabel = tk.Label(root, text="Výsledek")
canvas.create_window(350, 175, window = azimutLabel)

sklonLabel = tk.Label(root, text="sklon pukliny")
canvas.create_window(190, 200, window = sklonLabel)

azimutLabel = tk.Label(root, text="azimut pukliny")
canvas.create_window(190, 230, window = azimutLabel)

alpha_m = tk.Entry(root)
canvas.create_window(180, 50, window = alpha_m)

beta_m = tk.Entry(root)
canvas.create_window(180, 80, window = beta_m)

trend_m = tk.Entry(root)
canvas.create_window(570, 50, window = trend_m)

plunge_m = tk.Entry(root)
canvas.create_window(570, 80, window = plunge_m)

blank_1 = tk.Entry(root, state='readonly')
canvas.create_window(350, 200, window = blank_1)

blank_2 = tk.Entry(root, state='readonly')
canvas.create_window(350, 230, window = blank_2)

vypocitajButton = tk.Button(root,text='Vypočítej...',command=manual, font=('helvetica', 12, 'bold'))
canvas.create_window(350, 130, window=vypocitajButton)

#======================================= excel ==================================================

canvas.create_line(0, 280, 800, 280, fill="grey", width=1)

input_path = tk.Label(root, text="Nahrát vstupní soubor...")
canvas.create_window(350, 330, window = input_path)

input_entry = tk.Entry(root, text="", width=40)
canvas.create_window(350, 360, window = input_entry)

browse_1 = tk.Button(root, text="Vyhledat...", command=input)
canvas.create_window(550, 360, window = browse_1)

output_path = tk.Label(root, text="Uložit výstupní soubor...")
canvas.create_window(350, 420, window = output_path)

output_entry = tk.Entry(root, text="", width=40)
canvas.create_window(350, 450, window = output_entry)

browse_2 = tk.Button(root, text="Vyhledat...", command=output)
canvas.create_window(550, 450, window = browse_2)

calculate_button = tk.Button(root,text='Vypočítej a ulož...',command=vypocet, font=('helvetica', 12, 'bold'))
canvas.create_window(350, 500, window=calculate_button)

root.title("Orientátor")  
root.mainloop()
