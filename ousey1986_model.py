import numpy as np

# create domain
nrow = 11
ncol = 11
cellsize = 100
k = 2

# initial and boundary conditions
top = 9.
bottom = 5.
initial = 7.

heads = np.tile(initial, (ncol,nrow))
for i in range(ncol):        
	heads[0] = top
	heads[(nrow-1)] = bottom
print(heads)

# create wells matrix
wells = np.zeros((ncol, nrow))
wells[5, 5] = -0.65
print(wells)


print("Head matrix is a ", nrow, "by ", ncol, " matrix.")
iterations = 1
conv_crit = 1e-5
converged = False
while (not converged):

	for r in range(1, nrow - 1):
		for c in range(1, ncol - 1):
			heads_old = heads[r, c]
			heads[r, c] = (0 if ((heads[r - 1, c]**2 + heads[r + 1, c]**2 + heads[r, c - 1]**2 + heads[r, c + 1]**2) + (2*wells[r, c]*cellsize/k))/ 4. < 0 else ((heads[r - 1, c]**2 + heads[r + 1, c]**2 + heads[r, c - 1]**2 + heads[r, c + 1]**2) + (2*wells[r, c]*cellsize/k)) / 4.)**0.5    # relu
	
	#left boundary		
	for r in range(1, nrow - 1):
		c = 0
		heads_old = heads[r, c]
		heads[r, c] = 0 if ((heads[r - 1, c]**2 + heads[r + 1, c]**2 + 2*heads[r, c + 1]**2) / 4.)**0.5 < 0 else ((heads[r - 1, c]**2 + heads[r + 1, c]**2 + 2*heads[r, c + 1]**2) / 4.)**0.5 
		
	#right boundary
	for r in range(1, nrow -1):
		c = 10
		heads_old = heads[r, c]
		heads[r, c] = 0 if ((heads[r - 1, c]**2 + heads[r + 1, c]**2 + 2*heads[r, c - 1]**2) / 4.)**0.5 < 0 else ((heads[r - 1, c]**2 + heads[r + 1, c]**2 + 2*heads[r, c - 1]**2) / 4.)**0.5
		
	diff = heads[r, c] - heads_old
	diff2 = heads_old - heads[r, c]
	if (diff < conv_crit) and (diff2 < conv_crit):
		converged = True
	iterations += iterations 
print("Number of iterations = ", iterations - 1)
print(heads)

#plot results
import matplotlib.pyplot as plt
X = np.linspace(0, nrow*cellsize, nrow)
Y = np.linspace(nrow*cellsize, 0, nrow)
levels = 15
fig, ax = plt.subplots(1, 1)
cp = ax.contour(X, Y, heads, levels)
fig.colorbar(cp)
ax.set_title('Calculated heads')
ax.set_xlabel('X (m)')
ax.set_ylabel('Y (m)')
plt.show()
